// C++ program to play Tic-Tac-Toe
#include <string>
#include <stdlib.h>
#include <iostream>

// Length of the board
#define GRID_SIZE 3

using namespace std;

//initialize
string player1, player2;
int numPlayers = 0;

//Game class
class Game {

private:
	//define tictactoe initial grid
	char grid[GRID_SIZE][GRID_SIZE];

public:

    //1 player - user vs computer 
    // 2 players  - user vs user
	int countPlayers() {

		do {
			cout << "1 or 2 players? Enter 1 for 1 player, 2 for 2 players: ";
			cin >> numPlayers;

			if (numPlayers == 1) {
				cout << "Enter name for Player 1: ";
				cin >> player1;
				break;
			} else if (numPlayers == 2) {
				cout << "Enter name for Player 1: ";
				cin >> player1;

				cout << "Enter name for Player 2: ";
				cin >> player2;
				break;
			} else {
				cout << "Incorrect entry. Please try again." << endl;
			}
		} while (numPlayers != 1 && numPlayers != 2);

		return numPlayers;
	}

	void generateGrid() {
		//generate board in data form
		int number = 1;
		for (int x = 0; x < GRID_SIZE; x++) {
            /* to_string converts numeric value to string 
            c_str returns a const char* that points to a null-terminated string (i.e. a C-style string). 
            Pass the contents of string to array. */
			for (int y = 0; y < GRID_SIZE; y++) {
				grid[x][y] = to_string(number).c_str()[0];
				number += 1;
			}
		}
	}

	void showBoard() {
		//display board
		cout << " ------------\n";
		for (int x = 0; x < GRID_SIZE; x++) {
			cout << " |";
			for (int y = 0; y < GRID_SIZE; y++) {
				cout << " " << grid[x][y] << " |";
			}
			cout << "\n -------------\n";
		}

		return;
	}

	void checkWin() {
		// 8 possible ways of winning
		const char *winningMoves[8] = { "123", "456", "789", "147", "258",
				"369", "159", "753" };

		//loop through all of the possible movesets:

		for (int i = 0; i < 8; i++) {

			bool winner = true;
			//see previous character
			char previousBoard = '0';
            // use a pointer and go through each array and each element of that array
			const char *winningMove = winningMoves[i];

			for (int j = 0; j < 3; j++) {
                //go through each element of a winning array
				char character = winningMove[j];

				int gridSpace = (character - '0') - 1; // need to 0 base to determine row and column

				int row = gridSpace / GRID_SIZE; 
				int col = gridSpace % GRID_SIZE;

				char gridLocation = grid[row][col];

				if (previousBoard == '0') { //always the initial case
					//look at what we are looking at now
					previousBoard = gridLocation;
				} else if (previousBoard == gridLocation) {
					continue; // do it again - it might be a winning board
				} else {
					winner = false;
					break;
				}
			}

			if (winner) {
				cout << "********* We have a winner! ****" << endl;
				cout << previousBoard << " won! " << endl;

				exit(0);
			}
		}
	}

	//Player 1 turn
	void play() {

		int player1Input;

		//check for proper input
		while (true) {

			cout << player1 << ", Where would you like to place your move?"
					<< endl;
			cin >> player1Input;

			if (player1Input >= 1 && player1Input <= 9) {
				int index = (player1Input - 1); // 0 -8

				int row = index / 3;
				int col = index % 3;

				char position = grid[row][col];

				if (position == 'X' || position == 'O') {
					cout << "The position you selected is already taken."
							<< endl;
				} else {
					grid[row][col] = 'X';
					break;
				}

			} else {
				cout << "Please enter a valid entry. Only enter 1 to 9!"
						<< endl;
			}
		}

	}

	void computerTurn() {

		while (true) {
			int computerSelect = (rand() % 9) + 1; //max and main

			// 0 based
			int row = (computerSelect - 1) / 3;
			int col = (computerSelect - 1) % 3;

			//2d array
			char boardPosition = grid[row][col];

			if (boardPosition == 'X' || boardPosition == 'O') {
				//position already taken - try another random number
				continue;
			} else {
				cout << "Computer chose " << computerSelect << endl;
				grid[row][col] = 'O';
				break;
			}
		}
	}

	//Player 2 turn
	void player2Turn() {

		int player2Input;

		//check for proper input
		while (true) {

			cout << player2 << ", Where would you like to place your move?"
					<< endl;
			cin >> player2Input;

			if (player2Input >= 1 && player2Input <= 9) {
				int index = (player2Input - 1); // 0 -8

				int row = index / 3;
				int col = index % 3;

				char position = grid[row][col];

				if (position == 'X' || position == 'O') {
					cout << "The position you selected is already taken."
							<< endl;
				} else {
					grid[row][col] = 'O';
					break;
				}

			} else {
				cout << "Please enter a valid entry. Only enter 1 to 9!"
						<< endl;
			}
		}

	}

	//constructor

	Game() {

		countPlayers();
		generateGrid();

		showBoard();
		checkWin();

		while (true) {
			checkWin();

			if (numPlayers == 1) {
				play();
				computerTurn();
			} else {
				play();
				showBoard();
				player2Turn();
			}
			showBoard();
			checkWin();

		}
	}

};

void customBoard() {
	unsigned int rows = 0;
	unsigned int cols = 0;

	cout << "Enter number of rows: ";
	cin >> rows;

	cout << "Enter number of columns: ";
	cin >> cols;

	//allocate memory for the array
	int **array = new int*[rows];
	for (unsigned int i = 0; i < rows; i++) {
		array[i] = new int[cols];
	}

	//generate board in data form
	int number = 1;
	for (int x = 0; x < rows; x++) {

		for (int y = 0; y < cols; y++) {
			array[x][y] = number;
			number += 1;
		}
	}

	cout << "New Board is now: \n";
	cout << " ------------------\n"; // need to adjust this
	for (unsigned int x = 0; x < rows; x++) {
		cout << " |";
		for (unsigned int y = 0; y < cols; y++) {
			cout << " " << array[x][y] << " |";
		}
		cout << "\n -----------------\n"; // need to adjust this
	}

	// Deallocate memory:
	for (unsigned int i = 0; i < rows; i++) {
		delete[] array[i];
	}
	delete[] array;

	cin.ignore();
	cin.get();

}

void makeSelection() {
	int answer;

	do {
		cout << "Please make a selection: \n"
				"1 - Play using a 3x3 grid \n"
				"2 - Customize grid size \n"
				"3 - Use tokens (Future Enhancement)\n"
				"4 - Modify user interaction (Future Enhancement) \n"
				"5 - Exit " << endl;
		cin >> answer;

		if (answer == 1) {
			Game game;
		} else if (answer == 2) {
			customBoard();
		} else if (answer == 3) {
			cout << "Future implementation." << endl;
			// useTokens(); future implementation
		} else if (answer == 4) {
			cout << "Future implementation." << endl;
			// modifyInteract(); future implementation
		} else if (answer == 5) {
			cout << "Thank you for playing." << endl;
		} else {
			cout << "Make your selection again." << endl;
		}
	} while (answer < 1 || answer > 5);

}

// Driver
int main(int argc, char *argv[]) {

	makeSelection();

	return 0;

}

