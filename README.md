Programming Challenge: TicTacToe in C++ with 1 or 2 players using a 3x3 grid size.
Run C++ program in terminal using gcc compiler in Ubuntu

1. Open Terminal 
2. Check gcc version by typing:
    $ gcc -version

   If not available, Type command to install gcc or g++ compiler to install the essential C/C++ development libraries in Ubuntu
    $ sudo apt install build-essential
2.  Enter: 
    $ pwd 
3. Copy HTTPS link to TicTacToe repository and clone by typing: 
    $ git clone https://gitlab.com/ydavidson2021/tictactoe.git
4. To access main.cpp, enter: 
    $ cd tictactoe/
5. Compile the program using the command:
    $ g++ main.cpp
(Make sure to be in the same directory where the file has been saved. )
Default output will be a.out of the g++ main.cpp command. 
6. Run the binary (a.out) to execute:
    $./a.out
7. Enjoy the game and thank you for this opportunity!
